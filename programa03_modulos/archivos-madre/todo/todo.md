# Presentación

En 1492 Johannes Trithemius llevó a cabo una crítica a las nuevas
técnicas de reproducción de textos. En su [_Elogio de los amanuenses_](http://gen.lib.rus.ec/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382)
desestima a la imprenta por su intento de llevar libros baratos
y de baja calidad a las masas. Según su postura, el cuidado en
la copia manual de textos hechos por los monjes es incomparable
al trabajo realizado por las máquinas.

Si bien en la actualidad la crítica de Trithemius nos parece
desproporcionada ---tratándose incluso de un síndrome que se
caracteriza por el rechazo al cambio a partir de argumentos que
pretenden proteger una zona de confort y, por supuesto, los mecanismos
de poder vigentes---, es cierto que la calidad y la diversidad
de soportes que ahora llamamos libros han disminuido a la par
que su cantidad, tanto de número de obras como de ejemplares,
ha aumentado de manera estrepitosa. Lo que hoy en día conocemos
como «libro» es el producto de una paulatina homogeneización
en las técnicas y las metodologías empleadas para elaborarlos.
Es decir, desde el advenimiento de la imprenta los modos contemporáneos
de producción de publicaciones han apostado por una serie de
estandarizaciones en pos del aumento de la producción.

Esto deja la calidad de los objetos producidos en un segundo
plano o constreñida a las posibilidades tecnológicas reinantes.
De una u otra forma se condiciona la diversidad de soportes a
la aparente necesidad de objetos innovativos para hacer más eficiente
la acumulación de capital. Quizá Trithemius temía ---aunque forzando
esta interpretación a la luz de la crisis editorial actual---
la pérdida de la diversidad de las técnicas y las metodologías
de producción editorial, en particular aquellas cuya finalidad
no era económica, sino la conservación de una tradición.

El discurso ilustrado dotó de una justificación humanística a
estas nuevas técnicas de reproducción de textos a la par que
obtuvo beneficios por la amplificación de su discurso a través
de la producción de una mayor cantidad de sus textos impresos.
Lo que se perdió en calidad en cuanto al objeto, se ganó en la
cantidad de sujetos que ahora tienen acceso a su contenido. El
aumento de la producción se defendió como _el_ mecanismo para
la propagación de la lectura y, con ello, del conocimiento y
de la cultura.

Así como Thrithemius le tocó vivir en una etapa de transición
---donde hubo un súbito aumento en las posibilidades futuras
de su profesión pero con el costo de una crisis de sus modos
vigentes de producción---, hoy ya es prácticamente innegable
que la edición está pasando por una nueva etapa de transformación.
Este monje alemán vivió la mudanza de los procesos manuales a
los maquinales. Desde mediados de los ochenta nosotros estamos
advirtiendo la migración de tecnologías análogas a las digitales.

El papel y la tinta, aunque presente todavía de manera predominante
en la economía de la industria editorial, ya no es el soporte
primigenio por el cual se organizan los procesos de producción
de publicaciones. Son la corriente eléctrica y los bits los que
establecen la disposición los medios para la producción de publicaciones
impresas o digitales. El papel y la tinta ya no son elementos
regulativos en la publicación, sino una consecuencia de otro
paradigma que está siendo alimentado por las nuevas tecnologías
de la información y la comunicación ---tal vez para satisfacer
una demanda por el papel que, según, cada nueva generación empieza
a solicitar en menor medida---.

Estas transformaciones están provocando una tensión en el sector
editorial entre la preservación y la innovación. La polarización
no se hace esperar. Por un lado, personas semejantes a Thrithemius
defienden la pérdida de la calidad editorial en mano de las nuevas
técnicas y metodologías de producción editorial y, así, la búsqueda
de estrategias para conservar los mecanismos de poder vigentes.
Por ejemplo, el atropello de inspeccionar y calificar cualquier
soporte a partir de las cualidades de los impresos o, de manera
más desproporcionada, el desarrollo de tecnologías que permiten
un control en la difusión semejante al que se ejerce en la distribución
de publicaciones impresas, como es el caso del +++DRM+++ (_Digital
Rights Management_).

Por otro lado, y aunque con menos ímpetu que hace una década,
hay personas que cantan la muerte de los soportes impresos. Este
polo de la crisis apela a la emancipación de los antiguos aparatos
de producción y de reproducción de la cultura y el conocimiento,
de manera muy similar al llamado de la Ilustración que, a través
de la imprenta, difundió sus discursos de liberación. No más
instituciones «monolíticas», como la universidad o la editorial,
o cadenas simbólicas, como las restricciones del papel, sino
más personas cara a cara produciendo cultura y conocimiento «inmaterial».
Menos determinaciones y menos intermediarios para el acceso a
la palabra: más _individuos_ y más apertura, como los bazares.

Sin embargo, la situación económica y política actual no permite
agitar la bandera de la Ilustración sin antes tomar en cuenta
un patente peligro. Lo que en el siglo +++XVIII+++ fue un parteaguas
para la gestación de un nuevo orden social ---la burguesía y
el proletariado--- hoy en día se presta más a la conservación
de ese orden y no a su crisis o su superación. El desarrollo
tecnológico actual exige de constantes innovaciones y la diversificación
de soportes para mantener los índices de producción y para sustentar
una economía global que se basa en la acumulación de capital.

En el caso de la industria editorial, el abrigo de las nuevas
tecnologías y el aumento en la producción de nuevos soportes
no son a favor de una liberación de la profesión de sus antiguas
formas de organización, sino un camino para salvaguardar la economía
que esta industria ha hecho posible. No se trata de la liberación
de los profesionistas dedicados a la edición o de los lectores,
sino de la preservación del monopolio de una industria a través
de la diversificación y tecnificación de su infraestructura.

En este sentido es entendible que en lugar de técnicas o métodos,
el enfoque pedagógico actual del sector editorial sea principalmente
a través del ofrecimiento de «soluciones» y de la mutación del
objeto textual en «contenido». Las soluciones en las que se vuelcan
diversos posgrados, diplomados o talleres por lo general tienen
tras de sí una currícula oculta que hace de los profesionistas
los clientes dependientes de estas nuevas tecnologías. Como se
sabe, estos tipos de propiedad intelectual están custodiadas
por grandes consorcios editoriales o empresas tecnológicas. Una
pedagogía basada en programas computacionales y en _tips_ para
hacer más eficiente su uso es, a lo sumo, el desarrollo de habilidades
que subordinan al alumno a las posibilidades ofrecidas por los
desarrolladores de _software_ y la industria editorial.

El traslado discursivo del texto al contenido no solo es la búsqueda
de un eufemismo _ad hoc_ a lo que está en boga en los medios
de comunicación. Este cambio en el discurso implica una pérdida
de especificidad del producto textual para así pretender colocar
su producción en la misma posición en la que se encuentra el
desarrollo de otro tipo de objetos culturales, como el _software_,
el audio o el video. En esta homologación del libro a otro tipo
de economías se pierde el carácter distintivo de la profesión.

La currícula que se ofrece a continuación es una respuesta ante
las problemáticas expuestas aquí. Como podrá observarse, trata
a los procesos editoriales de una manera muy distintiva y ampliamente
técnica. Es decir, no pretende entender a la edición en su sentido
habitual ---como una profesión, un arte o una tradición---. En
esta currícula la edición es fundamentalmente metodología y técnica
sin volcarse de lleno a dar soporte a la ola de «innovación»
presente en el sin fin de eventos y capacitaciones que se organizan
en torno al libro. Para esta currícula una innovación de los
objetos producidos exige una reflexión sobre la dependencia tecnológica
imperante y una práctica que dé habilidades al profesionista
con el menor número de ataduras.

Esta currícula supone que un enfoque pedagógico sobre los métodos
y los modos de producción editorial ayudará al estudiante a mantener
un desarrollo progresivo de sus habilidades, independientemente
de los programas computacionales en boga. El costo a pagar por
ello es adentrarse más a lo que está supuesto en cada formato
digital e interfaz gráfica de trabajo. La curva de aprendizaje
es, sin duda, mayor a las que podrían encontrarse al momento
de aprender a usar _software_. Sin embargo, tiene mayores posibilidades
de no ser interrumpida por nuevas «soluciones» o el aumento en
la popularidad de programas de cómputo o soportes.

Es decir, la necesidad de mayor tiempo de aprendizaje tiene como
consecuencia un ahorro en los recursos humanos necesarios para
adaptarse a cada nuevo parámetro de la industria. Los efectos
más palpables de ello es que el estudiante de esta currícula
poco a poco será un profesionista más especializado, ahorrándose
así tiempo y dinero para su preparación.

A sabiendas de que esto por sí solo no cuestiona el aumento de
la productividad bajo las políticas y economías actuales, esta
currícula espera contribuir al debate con otro tipo de estrategia.
Una crítica sobre una profesión y una industria se vuelve ineficaz
si de manera previa no existe un dominio sobre las técnicas que
le dan cabida. Esta currícula supone que en cierto grado la polarización
en el sector editorial se debe a un amplio desconocimiento de
lo que está detrás de las técnicas aplicadas y de lo que otras
tecnologías podrían hacer posible. Un mayor dominio sobre este
terreno, tal como se pretende aquí, podrá ayudar a generar puntos
de encuentro y a matizar lo que ambas posturas defienden. Por
ese motivo, aunque el aumento en la productividad es uno de los
efectos de este programa, su énfasis en el uso de ciertas tecnologías
busca ofrecer al estudiante un panorama más integral de lo que
ahora está en juego cuando hablamos sobre edición de publicaciones.

Si la corriente eléctrica y los bits ahora configuran los modos
de producción editorial, no hay duda de que es necesario abordarlos,
sea para su especialización o su transformación. El profesionista
de la edición no dejará de ser cliente si no adquiere los conocimientos
y las habilidades para convertirse en su propio arquitecto.

# Mapa curricular

En la siguiente gráfica se ven las relaciones entre los módulos
que comprenden la currícula del presente programa. Por motivos
de clarificación, la forma de cada nodo corresponde a su tipo:

* Nodo circular: módulo práctico
* Nodo triangular: módulo teórico
* Nodo en diamante: módulo teórico-práctico

El tamaño de cada nodo es relativo a la carga horaria del módulo.
Además, los colores y diseños de los vectores señalan sus tipos
de relación:

* Vector grueso: relación necesaria
* Vector normal: relación recomendada
* Vector discontinuo: relación emparentada

Los módulos emparentados pueden interesar al estudiante, pero
carecen de relación causal para el cumplimiento de la currícula.
Los módulos con relación recomendada tienen nexos causales que
pueden obviarse, aunque su cumplimiento facilitaría la ejecución
del módulo condicionado. Por último, la relación necesaria es
un vínculo causal que no puede eludirse.

Cabe observar que en la ficha de cada módulo se abrevia el tipo
de relación. Por ejemplo, la relación `2e` del módulo 1 denota
una relación **e**mparentada con el módulo 2.

Los números de cada nodo hacen mención al módulo correspondiente:

1. Metodología de la edición ramificada
2. Lenguajes de marcado: +++MD+++ y +++HTML+++
3. Artículos académicos en +++XML JATS+++ I
4. Fundamentos de TeX
5. Conversión de documentos: +++MD, DOCX, HTML+++ y más
6. Pecas: herramientas editoriales desde la terminal
7. Digitalización: del impreso al digital
8. Mundo de los _ebooks_
9. Mundo de los _appbooks_
10. Flujo para la gestión de traducciones
11. Introducción al _software_ libre para editores
12. Scribus y tipografías libres I
13. Propiedad intelectual y derechos de autor
14. Bienes comunes y _anticopyright_
15. Licencias de uso para publicaciones
16. Bibliotecas libres: LibGen, Aaaaarg, Sci-Hub y más
17. Instalación virtual de +++GNU+++/Linux
18. Bash: introducción al uso de la terminal
19. Fundamentos de programación con Ruby
20. Fundamentos de RegEx
21. Diseño +++CSS+++ para publicaciones digitales
22. Respositorios Git: respaldo de información

![](../img/graph.png)

# Sumario

## 1. Metodología de la edición ramificada

Este módulo consistirá en explicar en qué consiste la «edición
ramificada» y cómo funciona para producir de manera automatizada,
y en segundos, diversos formatos ---como +++PDF+++, +++EPUB+++
y +++MOBI+++--- a partir de unos «archivos madre». En contraste
con la «edición tradicional» o «cíclica», en donde cada nuevo
formato requiere de uno previo para repetir varios de los procesos
habituales en la producción de publicaciones, la edición ramificada
realiza procesos independientes para cada uno de los formatos
finales. Las ventajas que esto conlleva son la mejora en la calidad
editorial y técnica de las publicaciones, un uso más eficiente
de los recursos y un menor tiempo de producción en comparación
con otros métodos empleados en la edición.

## 2. Lenguajes de marcado: +++MD+++ y +++HTML+++

Un lenguaje de marcado es una manera de darle estructura a un
texto. En este módulo aprenderemos a aplicar este tipo de sintaxis
a las publicaciones para que sean fáciles de leer y entendibles
para cualquier computadora. Para ello, se conocerán las ventajas
y desventajas de trabajar con Markdown y +++HTML+++, se pondrán
a prueba dependiendo de cada una de las necesidades del proyecto
editorial y se acostumbrará a trabajar las estructuras antes
que el diseño.

## 3. Artículos académicos en +++XML JATS+++ I

Hoy en día para la publicación de artículos académicos en repositorios
científicos es necesario producir archivos en formato +++XML+++
y +++PDF+++. Uno de los esquemas más populares es +++JATS+++,
el cual con ciertas modificaciones es usado por Sci+++ELO+++.
En este módulo se explicará la importancia de este tipo de formatos
y su esquema básico, así como las herramientas y las metodologías
que pueden emplearse para evitar hacer un doble trabajo ---la
estructuración del +++XML+++ y la maquetación del +++PDF+++---.
Con un enfoque de lo simple a lo complejo, se mostrará cómo es
posible pasar de lenguajes de marcado ligero, como Markdown,
a archivos +++XML+++ y, de ahí, a su importación para la producción
de +++PDF+++.

## 4. Fundamentos de TeX

En el mundo de la edición existen dos grandes metodologías para
la producción de publicaciones impresas. La más popular es el
_desktop publishing_ cuyo ejemplo son InDesign o Scribus. Pese
a su relativa corta curva de aprendizaje, este tipo de método
se vuelve problemático cuando se trata de automatizar o procesar
grandes volúmenes de texto. Desde los ochenta existe una solución
a esta dificultad: los sistemas de composición tipográfica. En
este módulo se verán los fundamentos de TeX, el sistema de composición
más robusto y popular. A partir del uso de macros, se explicará
y aplicará la estructura de un documento TeX. Además se describirán
el proceso y las herramientas necesarias para producir +++PDF+++
con este sistema de composición.

## 5. Conversión de documentos: +++MD, DOCX, HTML+++ y más

Hoy en día la conversión de documentos nos ahorra tiempo y esfuerzo.
Aquí se aprenderá a utilizar esta herramienta para pasar de un
formato a otro y obtener un mayor control sobre nuestro texto,
sin necesidad de tener conocimientos profundos sobre lenguajes
de marcado. La conversión de documentos se vuelve indispensable
cuando estamos hablando de un flujo de trabajo constante en el
que la prioridad es la agilización para la producción de múltiples
formatos.

## 6. Pecas: herramientas editoriales desde la terminal

Hacer un +++EPUB+++ nunca había sido tan fácil como lo es ahora.
En un entorno técnico y según el método ramificado de edición
es como surgió Pecas: una serie de *scripts* que automatizan
el quehacer editorial y que pueden marcar una diferencia en su
calidad. En este modulo se conocerá a fondo la utilización de
esta herramienta y el uso de sus parámetros según las necesidades
de cada proyecto editorial.

## 7. Digitalización: del impreso al digital

Este módulo se enfocará en el traslado del impreso al digital.
Se conocerán los pasos necesarios que nos facilitarán el trabajo
de la extracción del texto y la obtención de un +++PDF+++ a partir
del levantamiento de imágenes hechas con un escáner. Además,
se conocerán diferentes tipos de escáneres, el _software_ necesario
para el posprocesamiento y las medidas de calidad pertinentes
para la adquisición de un producto final.

## 8. Mundo de los _ebooks_

Un _ebook_ y un +++EPUB+++ no son lo mismo aunque comparten una
relación en común: la producción de una publicación electrónica.
Un +++EPUB+++ es un conjunto de documentos +++XHTML+++ comprimidos
en un archivo +++ZIP+++ para su portabilidad y su legibilidad.
En este módulo se hará un análisis teórico sobre el desarrollo
de las publicaciones estandarizadas y propietarias, como los
formatos de Amazon o de Apple.

## 9. Mundo de los _appbooks_

Con el advenimiento y desarrollo de las tecnologías _web_ nos
encontramos en un mundo diferente con respecto a la visualización
y el manejo de la información. En este contexto es donde nacen
las publicaciones no estandarizadas. Las aplicaciones y los _appbooks_
son producciones digitales que se complementan con el usuario
a través de una dinámica interactiva. Estas publicaciones se
basan en distintas tecnologías para ofrecernos la posibilidad
de desarrollar objetos digitales que brindan narrativas poco
convencionales y permiten nuevas experiencias de lectura. En
este módulo se describirán los tipos de publicación no estandarizada,
su funcionamiento y las herramientas más utilizadas para su elaboración.

## 10. Flujo para la gestión de traducciones

La traducción de textos puede ser un proceso tortuoso y de creciente
complejidad según la cantidad de idiomas a traducir. Sin embargo,
a través del uso de `gettext`, la biblioteca +++GNU+++ de internacionalización,
es posible generar archivos que faciliten la traducción. En este
módulo se explicará el proceso técnico para pasar de archivos
de procesamiento de texto a los formatos +++PO+++ necesarios
para la gestión y la coordinación de proyectos de traducción.
También se esclarecerán las ventajas de este flujo de trabajo
como son la traducción simultánea, la posibilidad de trabajo
en línea a través de Weblate y la capacidad de incorporar las
traducciones a diversos formatos de salida como son +++HTML+++,
+++MD+++ o documentos de texto procesado.

## 11. Introducción al _software_ libre para editores

Las metodologías más populares en la edición contemporánea han
sido acaparadas por dos grandes empresas y sus paqueterías de
_software_: Microsoft Office y Adobe Creative Cloud. Esto representa
una serie de problemas de índole económica, social, política
así como de producción y reproducción de la cultura y del conocimiento.
En este módulo se verán las posibilidades, límites, oportunidades
y riesgos de esta dependencia tecnológica o de sus alternativas:
el _software_ libre o el código abierto. Una vez expuesta la
problemática entre el _software_ libre y propietario en la edición,
se instalarán alternativas gratuitas, abiertas o libres de los
programas vendidos por Microsoft o Adobe.

## 12. Scribus y tipografías libres I

No todo es InDesign. En el enfoque del _desktop publishing_ también
existe una alternativa gratuita y libre para la maquetación de
textos: Scribus. En este módulo se describirán las ventajas y
desventajas de su uso para la producción de publicaciones en
relación con InDesign. Una parte importante de este enfoque es
el empleo diverso de fuentes, así que también se abarcará la
problemática entre las tipografías de pago y las libres. A través
de ejercicios simples se enseñará a instalar Scribus, a buscar
fuentes libres y a producir publicaciones de alta calidad sin
la necesidad de amplios recursos económicos o sin el ejercicio
de la piratería, al mismo tiempo que se ahorra espacio en el
disco duro y consumo de memoria +++RAM+++.

## 13. Propiedad intelectual y derechos de autor

En el campo de la producción cultural contemporánea se tiene
un conjunto de legislaciones cuyo discurso busca proteger la
producción como propiedad. Por ejemplo, las patentes, las marcas,
el diseño industrial, las denominaciones de origen y, por supuesto,
los derechos de autor. Pese a su complejidad jurídica y paulatino
robustecimiento cuantitativo y cualitativo, estas legislaciones
---que en conjunto se conocen como «propiedad intelectual»---
no tienen teorías congruentes que las justifiquen. En este módulo
se analizarán las propuestas teóricas para la protección de este
régimen de propiedad, con especial énfasis en los derechos de
autor.

## 14. Bienes comunes y _anticopyright_

Ante el engrosamiento de las legislaciones relativas a la propiedad
intelectual, varios académicos y activistas han puesto énfasis
en tratar las producciones culturales como bienes comunes. En
este módulo se analizarán las críticas, las propuestas y los
límites discursivos expuestos por grupos u organizaciones a favor
de los bienes comunes, como son los movimientos del _software_
y la cultura libres, las iniciativas del código y el acceso abiertos
y otras posturas críticas. La reflexión se llevará a cabo a través
de los conceptos de _copyright_, _copyleft_, _copyjustright_,
_copyfarleft_ y _anticopyright_.

## 15. Licencias de uso para publicaciones

Las licencias de uso son documentos que establecen la posibilidad
de reproducción de una obra como su copia, traducción o adaptación.
Este contrato «resguarda» la integridad total o parcial del producto
cultural, ya sea una pieza musical, artística, literaria o de
_software_. En este módulo se reflexionará sobre las licencias
de uso más populares y cuales son las ventajas y desventajas
de sus aplicaciones.

## 16. Bibliotecas libres: LibGen, Aaaaarg, Sci-Hub y más

Los repositorios privados constriñen el acceso y se valen de
plataformas «especializadas» solo solventables para ciertas instituciones.
En este contexto surgen las bibliotecas libres que combaten la
centralización y el control total de los derechos de autor por
parte del sector privado. En estos espacios se almacenan textos
literarios, filosóficos, feministas, anarquistas y científicos.
El trabajo de estas plataformas es muy similar porque comparten
un objetivo: la defensa del acceso abierto. En este módulo se
hará un recorrido sobre el surgimiento de estas librerías y se
conocerán los parámetros necesarios para subir o descargar obras.

## 17. Instalación virtual de +++GNU+++/Linux

El uso de +++GNU+++/Linux hoy en día es muy sencillo. Sin embargo,
por diversos motivos no es fácil la decisión de instalar un nuevo
sistema operativo. Debido a fines pedagógicos a veces la mejor
opción es el uso de un mismo entorno. En este módulo se instalará
una distribución virtual de +++GNU+++/Linux sobre el sistema
operativo de base; tan simple como instalar otro programa en
computadoras con Windows o macOS. La ventaja es el acceso a las
herramientas utilizadas en esta currícula para que el usuario
se enfoque en los objetivos de aprendizaje y en el descubrimiento
de las ventajas que ofrece +++GNU+++/Linux.

## 18. Bash: introducción al uso de la terminal

La producción de publicaciones y la gestión de proyectos editoriales
por lo general se dan a través de interfaces gráficas. Pese a
su facilidad de uso, esto provoca inconvenientes que afectan
la calidad de los productos editoriales. El empleo de la terminal
---es decir, la ausencia de entornos gráficos--- permite tener
un mayor control sobre los documentos y los flujos de trabajo.
En este módulo se familiarizará al usuario con la terminal Bash
y sus funciones más sencillas para tener un mayor control sobre
los proyectos.

## 19. Fundamentos de programación con Ruby

«La edición se hizo código». Esta frase, aunque abrupta, sintetiza
una cuestión rara vez visible en los procesos editoriales: no
importan los formatos empleados para la producción de publicaciones
ni los programas usados para generarlos, al final todo se reduce
a ceros y unos. Con el uso de Ruby, un lenguaje de programación,
se verán los fundamentos para empezar a desarrollar rutinas mediante
_scripts_ que permitan la simplificación y la automatización
de tareas monótonas llevadas a cabo en la edición.

## 20. Fundamentos de RegEx

Aquí el texto es una cadena de caracteres. RegEx es un lenguaje
y una herramienta muy poderosa que se puede implementar en la
edición si se le usa de manera adecuada. Las expresiones regulares
pueden ser muy útiles para extraer información a través de patrones.
En este módulo se aprenderá a limpiar un texto y a darle la estructura
correcta. Se mostrarán fórmulas básicas que ayuden a sustituir
información y a entender que el «buscar» y «reemplazar» puede
ofrecer una solución confiable si se tienen las bases para utilizarlos.

## 21. Diseño +++CSS+++ para publicaciones digitales

Las hojas de estilos +++CSS+++ dan pauta a un mejor diseño de
las páginas +++HTML+++. Estas nos ayudan a modificar colores,
tamaños, fuentes, fondos o sombras y la posición de distintos
elementos. Este módulo se enfocará en el desarrollo de una hoja
de estilos personalizada e indispensable para la obtención de
una publicación electrónica equilibrada y agradable a la vista.
Además, se enseñarán sus ventajas y desventajas, sus partes básicas
y su declaración en el archivo +++HTML+++.

## 22. Respositorios Git: respaldo de información

El respaldo de la información siempre ha sido un dolor de cabeza
para los editores. Con mil y un discos duros se cree tener una
solución, pero no son confiables cuando son demasiados colaboradores.
Una solución es Git, un programa de código abierto que realiza
un seguimiento preciso de los cambios en un proyecto, más conocido
en tecnologías de la información como un controlador de versiones.
Este módulo brindará las herramientas y los conocimientos necesarios
para utilizar repositorios Git en lugar de discos duros, nubes
---como Google Drive o Dropbox--- o sin fines de versiones últimas
de cada proyecto editorial. Se enseñará la configuración de un
repositorio y su mantenimiento, la subida y descarga de información
y la adición de colaboradores: todo de manera gratuita.

# 1. Metodología de la edición ramificada

* Horas: 3
* Tipo: teórico
* Relaciones: 2e, 5e, 6e, 8e

## Descripción

Este módulo consistirá en explicar en qué consiste la «edición
ramificada» y cómo funciona para producir de manera automatizada,
y en segundos, diversos formatos ---como +++PDF+++, +++EPUB+++
y +++MOBI+++--- a partir de unos «archivos madre». En contraste
con la «edición tradicional» o «cíclica», en donde cada nuevo
formato requiere de uno previo para repetir varios de los procesos
habituales en la producción de publicaciones, la edición ramificada
realiza procesos independientes para cada uno de los formatos
finales. Las ventajas que esto conlleva son la mejora en la calidad
editorial y técnica de las publicaciones, un uso más eficiente
de los recursos y un menor tiempo de producción en comparación
con otros métodos empleados en la edición.

## Objetivos

* Aprender la distinción entre la publicación estandarizada y la
  no-estandarizada.
* Conocer qué son y en qué se diferencian la edición tradicional,
  la cíclica y la ramificada.
* Reflexionar sobre los diez puntos metodológicos de la edición
  ramificada.
* Describir en qué consisten los archivos madres y qué tipo de
  formatos se utilizan para ellos.
* Dilucidar las posibilidades, los límites, las ventajas y las
  desventajas de la edición ramificada.

## Temas

1. Supuestos básicos de la edición ramificada.
2. Tipos de edición digital.
3. Tipos de edición digital estandarizada: tradicional, cíclica y
   ramificada.
4. Aspectos generales de la edición ramificada.
5. Diez puntos metodológicos de la edición ramificada.
6. El papel central de los archivos madre.
7. Formatos y sintaxis.
8. Ejemplos de la metodología ramificada.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadora con acceso a internet para ver las diapositivas.

# 2. Lenguajes de marcado: +++MD+++ y +++HTML+++

* Horas: 7
* Tipo: teórico-práctico
* Relaciones: 1e, 3e, 4e, 5e, 6e, 8e, 10e, 18e

## Descripción

Un lenguaje de marcado es una manera de darle estructura a un
texto. En este módulo aprenderemos a aplicar este tipo de sintaxis
a las publicaciones para que sean fáciles de leer y entendibles
para cualquier computadora. Para ello, se conocerán las ventajas
y desventajas de trabajar con Markdown y +++HTML+++, se pondrán
a prueba dependiendo de cada una de las necesidades del proyecto
editorial y se acostumbrará a trabajar las estructuras antes
que el diseño.

## Objetivos

* Aprender a utilizar editores de texto.
* Conocer qué son los lenguajes de marcado.
* Usar de sintaxis de Markdown.
* Usar de etiquetas básicas en +++HTML+++.
* Realizar una publicación con +++MD+++ o +++HTML+++.

## Temas

1. Cómo descargar y utilizar un editor de textos.
2. Nomenclatura para hacer uso de carpetas.
3. Introducción y sintaxis a Markdown.
4. Introducción y sintaxis a +++HTML+++. 
5. Elaboración de una publicación.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 3. Artículos académicos en +++XML JATS+++ I

* Horas: 5
* Tipo: teórico-práctico
* Relaciones: 2e, 2r, 4e, 5e, 5r, 6e, 8e, 17r

## Descripción

Hoy en día para la publicación de artículos académicos en repositorios
científicos es necesario producir archivos en formato +++XML+++
y +++PDF+++. Uno de los esquemas más populares es +++JATS+++,
el cual con ciertas modificaciones es usado por Sci+++ELO+++.
En este módulo se explicará la importancia de este tipo de formatos
y su esquema básico, así como las herramientas y las metodologías
que pueden emplearse para evitar hacer un doble trabajo ---la
estructuración del +++XML+++ y la maquetación del +++PDF+++---.
Con un enfoque de lo simple a lo complejo, se mostrará cómo es
posible pasar de lenguajes de marcado ligero, como Markdown,
a archivos +++XML+++ y, de ahí, a su importación para la producción
de +++PDF+++.

## Objetivos

* Conocer qué es un archivo +++XML+++.
* Aprender qué es un esquema +++XML+++, con énfasis en los esquemas
  +++JATS+++ y Sci+++ELO+++.
* Explicar un enfoque de trabajo que permita incluir en un mismo
  ciclo de producción el desarrollo de archivos +++XML+++ y +++PDF+++.

## Temas

1. ¿Qué son los archivos +++XML+++ y dónde se localizan dentro
   de la familia +++HTML+++?
2. ¿Qué son los esquemas +++XML+++ +++JATS+++ y Sci+++ELO+++?
3. Propuesta metodológica de lo simple a lo complejo: de +++MD+++
   a +++XML+++ y +++PDF+++ hecho con InDesign o TeX.
4. Realización de ejercicios básicos para obtener archivos +++XML+++
   y +++PDF+++.

## Conocimientos previos

* Estar familiarizado con los lenguajes de marcado.
* Comprender la relevancia general de los repositorios científicos.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 4. Fundamentos de TeX

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 2e, 2r, 3e, 5e, 5r, 6e, 8e, 12e, 17r

## Descripción

En el mundo de la edición existen dos grandes metodologías para
la producción de publicaciones impresas. La más popular es el
_desktop publishing_ cuyo ejemplo son InDesign o Scribus. Pese
a su relativa corta curva de aprendizaje, este tipo de método
se vuelve problemático cuando se trata de automatizar o procesar
grandes volúmenes de texto. Desde los ochenta existe una solución
a esta dificultad: los sistemas de composición tipográfica. En
este módulo se verán los fundamentos de TeX, el sistema de composición
más robusto y popular. A partir del uso de macros, se explicará
y aplicará la estructura de un documento TeX. Además se describirán
el proceso y las herramientas necesarias para producir +++PDF+++
con este sistema de composición.

## Objetivos

* Aprender las diferencias entre el _desktop publishing_ y los
  sistemas de composición tipográfica.
* Comprender qué son los macros de LaTeX.
* Configurar un documento básico de LaTeX.
* Modificar diseño de las plantillas por defecto de LaTeX.

## Temas

1. Ventajas y desventajas del _desktop publishing_.
2. ¿Qué es un sistema de composición tipográfica?
3. ¿Qué es TeX?
4. ¿Qué es LaTeX?
5. Configuración de un documento con TeXstudio.
6. Modificaciones del diseño por defecto de LaTeX.
7. Búsqueda e implementación de plantillas.
8. Generación de +++PDF+++ a través de `lualatex`.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 5. Conversión de documentos: +++MD, DOCX, HTML+++ y más

* Horas: 3
* Tipo: práctico
* Relaciones: 1e, 2e, 3e, 4e, 6r, 8e, 18n, 20e, 22e

## Descripción

Hoy en día la conversión de documentos nos ahorra tiempo y esfuerzo.
Aquí se aprenderá a utilizar esta herramienta para pasar de un
formato a otro y obtener un mayor control sobre nuestro texto,
sin necesidad de tener conocimientos profundos sobre lenguajes
de marcado. La conversión de documentos se vuelve indispensable
cuando estamos hablando de un flujo de trabajo constante en el
que la prioridad es la agilización para la producción de múltiples
formatos.

## Objetivos

* Hacer la instalación de Pandoc y Pecas.
* Conocer los fundamentos básicos de Pandoc.
* Saber cuáles son los parámetros necesarios para la conversión. 
* Reflexionar sobre las características de cada archivo y detectar cuál es el más propicio dependiendo del proyecto editorial. 

## Temas

1. ¿Qué es Pandoc?
2. ¿Por qué Pandog y no Pandoc?
3. Parámetros necesarios para la conversión.
4. Ejemplos de conversión de archivos.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 6. Pecas: herramientas editoriales desde la terminal

* Horas: 6
* Tipo: práctico
* Relaciones: 1e, 2e, 3e, 4e, 7e, 8e, 17r, 18n, 22e

## Descripción

Hacer un +++EPUB+++ nunca había sido tan fácil como lo es ahora.
En un entorno técnico y según el método ramificado de edición
es como surgió Pecas: una serie de *scripts* que automatizan
el quehacer editorial y que pueden marcar una diferencia en su
calidad. En este modulo se conocerá a fondo la utilización de
esta herramienta y el uso de sus parámetros según las necesidades
de cada proyecto editorial.

## Objetivos

* Instalar Pecas.
* Introducir cada una de las herramientas editoriales que conforman Pecas.
* Conocer los parámetros necesarios para su funcionamiento.
* Aprender qué son los metadatos y cuál es su sintaxis correcta.
* Reflexionar acerca de esta metodología.
* Comprender las ventajas al optar por este flujo de trabajo.

## Temas

1. ¿Qué es Pecas?
2. ¿Qué es un árbol de directorios?
3. Rutas relativas.
4. Revisión de parámetros: de lo sencillo a lo complejo.
5. ¿Qué es el archivo +++YAML+++ y cómo es su sintaxis?
6. Verificación de +++EPUB+++.
7. Estado de Pecas y sus dependencias: `pc-doctor`.

## Conocimientos previos

* Uso básico de la terminal.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 7. Digitalización: del impreso al digital

* Horas: 5
* Tipo: teórico-práctico
* Relaciones: 6e, 8e, 11r, 16e, 22e

## Descripción

Este módulo se enfocará en el traslado del impreso al digital.
Se conocerán los pasos necesarios que nos facilitarán el trabajo
de la extracción del texto y la obtención de un +++PDF+++ a partir
del levantamiento de imágenes hechas con un escáner. Además,
se conocerán diferentes tipos de escáneres, el _software_ necesario
para el posprocesamiento y las medidas de calidad pertinentes
para la adquisición de un producto final.

## Objetivos

* Instalar ScanTailor.
* Instalar Simple Scan.
* Realizar la nomenclatura correcta en las carpetas.
* Aprender a hacer un levantamiento de imágenes.
* Llevar a cabo el posprocesamiento de imágenes.
* Hacer un reconocimiento de caracteres y obtener un archivo de texto.
* Conocer las herramientas para compilar archivos +++PDF+++.
* Reflexionar sobre la compartición de materiales y acercarnos a las bibliotecas libres que lo permiten.

## Temas

1. ¿Qué es ScanTailor y cómo funciona?
2. Cómo nombrar carpetas
3. ¿Qué es el posprocesamiento de imágenes?
4. Uso de +++OCR+++ para la obtención de texto.
5. Compilación de imágenes a un +++PDF+++ final.
6. Ejemplos bibliotecas libres. 

## Conocimientos previos

Ninguno.

## Materiales necesarios

* Escáner de cama plana.
* Computadoras para instalar los programas.
* Conexión a internet.
* Cañón.

# 8. Mundo de los _ebooks_

* Horas: 2
* Tipo: teórico
* Relaciones: 1e, 2e, 3e, 4e, 5e, 6e, 7e, 9e, 11e, 16e, 19e, 20e, 21e, 22e

## Descripción

Un _ebook_ y un +++EPUB+++ no son lo mismo aunque comparten una
relación en común: la producción de una publicación electrónica.
Un +++EPUB+++ es un conjunto de documentos +++XHTML+++ comprimidos
en un archivo +++ZIP+++ para su portabilidad y su legibilidad.
En este módulo se hará un análisis teórico sobre el desarrollo
de las publicaciones estandarizadas y propietarias, como los
formatos de Amazon o de Apple.

## Objetivos

* Aprender qué es una publicación estandarizada.
* Conocer la diferencia entre los tipos de _ebooks_.
* Mostrar herramientas para hacer _ebooks_.

## Temas

1. ¿Qué es una publicación estandarizada?
2. ¿Qué es un _ebook_?
3. ¿Qué es un +++EPUB+++?
4. ¿Cuáles otros formatos pueden ser _ebook_?
5. ¿Cuáles herramientas existen para producir _ebooks_?
6. ¿Dónde se puede distribuir _ebooks_?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.

# 9. Mundo de los _appbooks_

* Horas: 2
* Tipo: teórico
* Relaciones: 8e, 11e, 19e, 20e

## Descripción

Con el advenimiento y desarrollo de las tecnologías _web_ nos
encontramos en un mundo diferente con respecto a la visualización
y el manejo de la información. En este contexto es donde nacen
las publicaciones no estandarizadas. Las aplicaciones y los _appbooks_
son producciones digitales que se complementan con el usuario
a través de una dinámica interactiva. Estas publicaciones se
basan en distintas tecnologías para ofrecernos la posibilidad
de desarrollar objetos digitales que brindan narrativas poco
convencionales y permiten nuevas experiencias de lectura. En
este módulo se describirán los tipos de publicación no estandarizada,
su funcionamiento y las herramientas más utilizadas para su elaboración.

## Objetivos

* Aprender qué es una publicación no estandarizada.
* Conocer la diferencia entre los tipos de _appbooks_.
* Mostrar herramientas para hacer _appbooks_.

## Temas

1. ¿Qué es una publicación no estandarizada?
2. ¿Qué es un _appbook_?
3. ¿Cuáles formatos pueden ser _appbook_?
4. ¿Cuáles herramientas existen para producir _appbooks_?
5. ¿Dónde se puede distribuir _appbooks_?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.

# 10. Flujo para la gestión de traducciones

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 2e, 11r, 18n, 19r, 22e

## Descripción

La traducción de textos puede ser un proceso tortuoso y de creciente
complejidad según la cantidad de idiomas a traducir. Sin embargo,
a través del uso de `gettext`, la biblioteca +++GNU+++ de internacionalización,
es posible generar archivos que faciliten la traducción. En este
módulo se explicará el proceso técnico para pasar de archivos
de procesamiento de texto a los formatos +++PO+++ necesarios
para la gestión y la coordinación de proyectos de traducción.
También se esclarecerán las ventajas de este flujo de trabajo
como son la traducción simultánea, la posibilidad de trabajo
en línea a través de Weblate y la capacidad de incorporar las
traducciones a diversos formatos de salida como son +++HTML+++,
+++MD+++ o documentos de texto procesado.

## Objetivos

* Comprender la importancia de `gettext` y los archivos +++PO+++
  para la gestión de traducciones.
* Aprender la dinámica de trabajo de +++MD+++ a +++PO+++ para
  concluir en archivos +++MD+++, +++HTML+++, +++TEX+++ o +++PDF+++.
* Instalar Poedit.
* Usar la plataforma Weblate.
* Realizar ejercicios de traducción para poner en práctica la
  metodología.

## Temas

1. ¿Qué es `gettext` y los archivos +++PO+++?
2. Instalación de Poedit.
3. Suscripción a la plataforma Weblate.
4. Estructuración del texto a Markdown.
5. Conversión de +++MD+++ a archivos +++PO+++.
6. Traducción a partir de archivos +++PO+++ usando Poedit o Weblate.
7. Conversión de archivos +++PO+++ a +++MD+++, +++HTML+++, +++TEX+++
   o +++PDF+++.

## Conocimientos previos

* Uso de la terminal Bash.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 11. Introducción al _software_ libre para editores

* Horas: 3
* Tipo: teórico-práctico
* Relaciones: 8e, 9e, 12e, 17e, 22e

## Descripción

Las metodologías más populares en la edición contemporánea han
sido acaparadas por dos grandes empresas y sus paqueterías de
_software_: Microsoft Office y Adobe Creative Cloud. Esto representa
una serie de problemas de índole económica, social, política
así como de producción y reproducción de la cultura y del conocimiento.
En este módulo se verán las posibilidades, límites, oportunidades
y riesgos de esta dependencia tecnológica o de sus alternativas:
el _software_ libre o el código abierto. Una vez expuesta la
problemática entre el _software_ libre y propietario en la edición,
se instalarán alternativas gratuitas, abiertas o libres de los
programas vendidos por Microsoft o Adobe.

## Objetivos

* Comprender las posibilidades y los límites del uso de tecnologías
  propietarias.
* Aprender sobre las oportunidades y los riesgos del _software_ libre
  o del código abierto.
* Instalar programas alternativos a los ofrecidos por Microsoft
  o Adobe.

## Temas

1. Adobe y Microsoft, los grandes hermanos.
2. Lo gratuito, lo abierto y lo libre.
3. ¿Qué es el _software_ libre y qué es el código abierto (+++FOSS+++)?
4. +++FOSS+++ en la edición, ¿para qué?
5. Herramientas editoriales: Scribus, TeX, Pecas y más.
6. Herramientas de diseño: Gimp, Inkscape y Krita.
7. Herramientas de comunicación y gestión: Telegram, pads y
   Nextcloud.
8. Herramientas de digitalización: LibreScan, ScanTailor y Tesseract.
9. Servidores de correo alternativos: Riseup y Disroot.
10. Plataformas de difusión: las redes federadas.
11. Plataformas de distribución: LibGen, Aaaaarg y Leanpub.
12. Giro completo en el +++FOSS+++: alternativas a Windows y macOS.
13. Algunas navajas suizas: Pandoc, Git, +++SSH+++, Xpdf y `libtiff`.

## Conocimientos previos

* Usar o tener nociones básicas del _software_ ofrecido por Microsoft
  o Adobe.

## Materiales necesarios

* Computadoras para instalar los programas.
* Conexión a internet.
* Cañón.

# 12. Scribus y tipografías libres I

* Horas: 10
* Tipo: teórico-práctico
* Relaciones: 4e, 11e, 22e

## Descripción

No todo es InDesign. En el enfoque del _desktop publishing_ también
existe una alternativa gratuita y libre para la maquetación de
textos: Scribus. En este módulo se describirán las ventajas y
desventajas de su uso para la producción de publicaciones en
relación con InDesign. Una parte importante de este enfoque es
el empleo diverso de fuentes, así que también se abarcará la
problemática entre las tipografías de pago y las libres. A través
de ejercicios simples se enseñará a instalar Scribus, a buscar
fuentes libres y a producir publicaciones de alta calidad sin
la necesidad de amplios recursos económicos o sin el ejercicio
de la piratería, al mismo tiempo que se ahorra espacio en el
disco duro y consumo de memoria +++RAM+++.

## Objetivos

* Instalar Scribus.
* Aprender a configurar un documento en Scribus.
* Conocer qué son las páginas maestras.
* Maquetar un texto con Scribus.
* Comprender qué son las tipografías libres.
* Buscar e instalar tipografías libres.
* Usar tipografías libres en Scribus.
* Producir un +++PDF+++.

## Temas

1. ¿Qué es Scribus, la alternativa libre a InDesign?
2. Configuración y entorno de trabajo de Scribus.
3. ¿Qué son las tipografías libres?
4. Instalación y uso de tipografías libres.
5. Diseño de páginas maestras en Scribus.
6. Maquetación de texto en Scribus.
7. Producción de +++PDF+++ con Scribus.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para descargar e instalar Scribus.
* Conexión a internet.
* Cañón.

# 13. Propiedad intelectual y derechos de autor

* Horas: 10
* Tipo: teórico
* Relaciones: 14e, 15e, 16e

## Descripción

En el campo de la producción cultural contemporánea se tiene
un conjunto de legislaciones cuyo discurso busca proteger la
producción como propiedad. Por ejemplo, las patentes, las marcas,
el diseño industrial, las denominaciones de origen y, por supuesto,
los derechos de autor. Pese a su complejidad jurídica y paulatino
robustecimiento cuantitativo y cualitativo, estas legislaciones
---que en conjunto se conocen como «propiedad intelectual»---
no tienen teorías congruentes que las justifiquen. En este módulo
se analizarán las propuestas teóricas para la protección de este
régimen de propiedad, con especial énfasis en los derechos de
autor.

## Objetivos

* Conocer qué se entiende por «propiedad intelectual».
* Aprender las diferencias entre los derechos de autor y el
  _copyright_.
* Estudiar la teoría progresista, personalista y laborista.
* Reflexionar sobre los supuestos y los límites de la propiedad
  intelectual.

## Temas

1. Morfología de la propiedad intelectual.
2. En la búsqueda de una definición y una teoría de la propiedad
   intelectual.
3. La teoría progresista: el utilitarismo de la legislación
   estadunidense.
4. La teoría personalista: Hegel, Kant y ¿más Hegel?
5. La teoría laborista: la propiedad de Locke.
6. Vertientes continental y anglosajona de la propiedad literaria:
   los derechos de autor y el _copyright_.
7. ¿La producción es propiedad?: crítica de Proudhon a la propiedad
   literaria.
8. La producción cultural y la reproducción de capital: la
   crítica de Walter Benjamin al autor y la obra.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Ninguno.

# 14. Bienes comunes y _anticopyright_

* Horas: 10
* Tipo: teórico
* Relaciones: 13e, 15e, 16e

## Descripción

Ante el engrosamiento de las legislaciones relativas a la propiedad
intelectual, varios académicos y activistas han puesto énfasis
en tratar las producciones culturales como bienes comunes. En
este módulo se analizarán las críticas, las propuestas y los
límites discursivos expuestos por grupos u organizaciones a favor
de los bienes comunes, como son los movimientos del _software_
y la cultura libres, las iniciativas del código y el acceso abiertos
y otras posturas críticas. La reflexión se llevará a cabo a través
de los conceptos de _copyright_, _copyleft_, _copyjustright_,
_copyfarleft_ y _anticopyright_.

## Objetivos

* Conocer una breve historia del movimiento contemporáneo en pos
  de los bienes comunes.
* Aprender las diferencias entre _copyright_, _anticopyright_,
  _copyleft_, _copyjustright_ y _copyfarleft_.
* Reflexionar sobre los supuestos, las posibilidades y los límites
  de los movimientos e iniciativas en pos de los bienes comunes.

## Temas

1. El familiar incómodo de la propiedad intelectual: los bienes
   comunes.
2. El resurgimiento de la defensa a los bienes comunes: el
   _software_ libre.
3. La bifurcación: el código abierto.
4. La amplificación: la cultura libre y el acceso abierto.
5. La crítica interna: el _copyfarleft_.
6. En la búsqueda de los bienes comunes.
7. Historización: ¿qué puede decirnos Proudhon y Walter Benjamin
   en pos de los bienes comunes?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Ninguno.

# 15. Licencias de uso para publicaciones

* Horas: 5
* Tipo: teórico
* Relaciones: 13e, 14e, 16e

## Descripción

Las licencias de uso son documentos que establecen la posibilidad
de reproducción de una obra como su copia, traducción o adaptación.
Este contrato «resguarda» la integridad total o parcial del producto
cultural, ya sea una pieza musical, artística, literaria o de
_software_. En este módulo se reflexionará sobre las licencias
de uso más populares y cuales son las ventajas y desventajas
de sus aplicaciones.

## Objetivos

* Conocer los diferentes tipos de licencias.
* Explicar los componentes de una licencia.
* Comprender cuál es la importancia de su uso.
* Reflexionar por qué se siguen utilizando y sus implicaciones. 

## Temas

1. ¿Qué es una licencia de uso?
2. ¿Qué es el _copyright_ y cómo funciona?
3. Otras opciones: Creative Commons, _copyfarleft_, Licencia de pares.
5. +++LEAL+++, una alternativa editorial radical.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.

# 16. Bibliotecas libres: LibGen, Aaaaarg, Sci-Hub y más

* Horas: 4 
* Tipo: teórico-práctico
* Relaciones: 7e, 8e, 13e, 14e, 15e

## Descripción

Los repositorios privados constriñen el acceso y se valen de
plataformas «especializadas» solo solventables para ciertas instituciones.
En este contexto surgen las bibliotecas libres que combaten la
centralización y el control total de los derechos de autor por
parte del sector privado. En estos espacios se almacenan textos
literarios, filosóficos, feministas, anarquistas y científicos.
El trabajo de estas plataformas es muy similar porque comparten
un objetivo: la defensa del acceso abierto. En este módulo se
hará un recorrido sobre el surgimiento de estas librerías y se
conocerán los parámetros necesarios para subir o descargar obras.

## Objetivos

* Conocer cuáles son las bibliotecas libres.
* Aprender a subir publicaciones.
* Aprender a bajar información.
* Reflexionar sobre la liberación de contenidos a través de estos espacios.

## Temas

1. ¿Qué es una biblioteca libre?
2. Cómo subir y bajar material de estas plataformas.
3. ¿Qué tipo de formatos permite subir o descargar? 
4. Comprensión de la labor e importancia de estas bibliotecas.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 17. Instalación virtual de +++GNU+++/Linux

* Horas: 2
* Tipo: práctico
* Relaciones: 11e

## Descripción

El uso de +++GNU+++/Linux hoy en día es muy sencillo. Sin embargo,
por diversos motivos no es fácil la decisión de instalar un nuevo
sistema operativo. Debido a fines pedagógicos a veces la mejor
opción es el uso de un mismo entorno. En este módulo se instalará
una distribución virtual de +++GNU+++/Linux sobre el sistema
operativo de base; tan simple como instalar otro programa en
computadoras con Windows o macOS. La ventaja es el acceso a las
herramientas utilizadas en esta currícula para que el usuario
se enfoque en los objetivos de aprendizaje y en el descubrimiento
de las ventajas que ofrece +++GNU+++/Linux.

## Objetivos

* Instalar VirtualBox en cualquier sistema operativo, incluyendo
  Windows o macOS.
* Instalar mediante VirtualBox un clon de Debian ---una distribución
  de +++GNU+++/Linux--- que incluye las configuraciones y las
  herramientas necesarias para esta currícula.
* Instalar las _guest additions_ para aumentar la usabilidad, como
  es la posibilidad de carpetas compartidas.
* Entender de manera somera la dinámica _guest/host_: cómo se
  relaciona el sistema operativo virtual con el sistema operativo
  de base.

## Temas

1. ¿Qué es una máquina virtual y qué es VirtualBox?
2. ¿Qué es una distribución de +++GNU+++/Linux y Debian?
3. ¿Qué son las máquinas _guest_ y _host_?
4. Descarga e instalación de VirtualBox.
5. Descarga e instalación de un clon de Debian.
6. Descarga e instalación de _guest additions_.
7. Configuración de carpetas compartidas.

## Conocimientos previos

* Conocimiento de descarga e instalación de programas en el sistema
  operativo en uso.

## Materiales necesarios

* Una computadora con al menos 6 +++GB+++ de espacio.
* Una conexión a internet.

# 18. Bash: introducción al uso de la terminal

* Horas: 5
* Tipo: práctico
* Relaciones: 2e, 17r

## Descripción

La producción de publicaciones y la gestión de proyectos editoriales
por lo general se dan a través de interfaces gráficas. Pese a
su facilidad de uso, esto provoca inconvenientes que afectan
la calidad de los productos editoriales. El empleo de la terminal
---es decir, la ausencia de entornos gráficos--- permite tener
un mayor control sobre los documentos y los flujos de trabajo.
En este módulo se familiarizará al usuario con la terminal Bash
y sus funciones más sencillas para tener un mayor control sobre
los proyectos.

## Objetivos

* Instalar y configurar la terminal Bash.
* Aprender comandos básicos de Bash.
* Hacer ejercicios para reforzar el aprendizaje.

## Temas

1. ¿Qué son la interfaz de línea de comandos y Bash?
2. Instalación y configuración de Bash.
3. Uso de `ls`: lista de ficheros.
4. Uso de `pwd`: impresión de la ubicación.
5. Uso de `mkdir`: creación de directorios.
6. Uso de `cd`: cambio de directorio. 
7. Uso de `nano`: creación y edición de archivos. 
8. Uso de `cat`: exhibición de archivos y más. 
9. Uso de `mv`: movimiento o renombre de ficheros. 
10. Uso de `cp`: copia de ficheros. 
11. Uso de `rm`: eliminación de ficheros. 
12. Uso de `clear`: limpieza de la terminal. 

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras con Bash o +++GNU+++/Linux instalado.
* Conexión a internet.
* Cañón.

# 19. Fundamentos de programación con Ruby

* Horas: 20
* Tipo: práctico
* Relaciones: 5r, 6r, 8e, 9e, 18n, 20e

## Descripción

«La edición se hizo código». Esta frase, aunque abrupta, sintetiza
una cuestión rara vez visible en los procesos editoriales: no
importan los formatos empleados para la producción de publicaciones
ni los programas usados para generarlos, al final todo se reduce
a ceros y unos. Con el uso de Ruby, un lenguaje de programación,
se verán los fundamentos para empezar a desarrollar rutinas mediante
_scripts_ que permitan la simplificación y la automatización
de tareas monótonas llevadas a cabo en la edición.

## Objetivos

* Instalar y configurar Ruby.
* Aprender los fundamentos de programación a través de Ruby.
* Programar _scripts_ para automatizar el procesamiento de texto.
* Usar _scripts_ como programas de terminal.

## Temas

1. ¿Qué son los lenguajes de programación y Ruby?
2. Instalación y configuración de Ruby.
3. Números, líneas de textos y variables.
4. Conversión de variables y algunos métodos de Ruby.
5. Condicionales.
6. Matrices e iteraciones.
7. Clase `Hash` de Ruby.
8. Creación de métodos.
9. Extensión de clases.
10. Uso de gemas y de otras herramientas en Ruby.
11. Formateo de texto.
13. Conversión de formatos de texto.
14. Desarrollo de _scripts_ de automatización.
15. De _script_ a programa de la terminal.

## Conocimientos previos

* Uso de la terminal Bash.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 20. Fundamentos de RegEx

* Horas: 10 
* Tipo: práctico
* Relaciones: 2r, 5e, 8e, 9e, 19e

## Descripción

Aquí el texto es una cadena de caracteres. RegEx es un lenguaje
y una herramienta muy poderosa que se puede implementar en la
edición si se le usa de manera adecuada. Las expresiones regulares
pueden ser muy útiles para extraer información a través de patrones.
En este módulo se aprenderá a limpiar un texto y a darle la estructura
correcta. Se mostrarán fórmulas básicas que ayuden a sustituir
información y a entender que el «buscar» y «reemplazar» puede
ofrecer una solución confiable si se tienen las bases para utilizarlos.

## Objetivos

* Conocer qué son las expresiones regulares. 
* Explicar la nomenclatura de RegEx.
* Describir fórmulas para la limpieza de formato.

## Temas

1. Todo esencialmente es un carácter.
2. ¿Qué significa cada carácter en RegEx?
3. Buscar y reemplazar todo.
4. Fórmulas para limpiar y formatear texto. 

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 21. Diseño +++CSS+++ para publicaciones digitales

* Horas: 7 
* Tipo: práctico
* Relaciones: 2r, 8e

## Descripción

Las hojas de estilos +++CSS+++ dan pauta a un mejor diseño de
las páginas +++HTML+++. Estas nos ayudan a modificar colores,
tamaños, fuentes, fondos o sombras y la posición de distintos
elementos. Este módulo se enfocará en el desarrollo de una hoja
de estilos personalizada e indispensable para la obtención de
una publicación electrónica equilibrada y agradable a la vista.
Además, se enseñarán sus ventajas y desventajas, sus partes básicas
y su declaración en el archivo +++HTML+++.

## Objetivos

* Conocer los atributos de una hoja de estilos.
* Vincular hojas +++CSS+++ con el archivo +++HTML+++.
* Explicar los atributos y valores.
* Usar la sintaxis correcta para la aplicación de elementos.
* Mostrar algunos atributos.

## Temas

1. Cómo declarar nuestro archivo +++CSS+++.
2. Selectores y reglas visuales.
3. Sintaxis de propiedades +++CSS+++.
4. Colores y tipografías.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.

# 22. Respositorios Git: respaldo de información

* Horas: 15
* Tipo: teórico-práctico
* Relaciones: 5e, 6e, 7e, 8e, 10e, 11e, 12e, 18n

## Descripción

El respaldo de la información siempre ha sido un dolor de cabeza
para los editores. Con mil y un discos duros se cree tener una
solución, pero no son confiables cuando son demasiados colaboradores.
Una solución es Git, un programa de código abierto que realiza
un seguimiento preciso de los cambios en un proyecto, más conocido
en tecnologías de la información como un controlador de versiones.
Este módulo brindará las herramientas y los conocimientos necesarios
para utilizar repositorios Git en lugar de discos duros, nubes
---como Google Drive o Dropbox--- o sin fines de versiones últimas
de cada proyecto editorial. Se enseñará la configuración de un
repositorio y su mantenimiento, la subida y descarga de información
y la adición de colaboradores: todo de manera gratuita.

## Objetivos

* Conocer el esquema de trabajo de Git.
* Aprender a crear y clonar repositorios.
* Explicar los comandos básicos de Git.
* Reflexionar sobre las ventajas de este modelo de trabajo.

## Temas

1. ¿Qué es un control de versiones?
2. ¿Cómo crear mi primer repositorio? 
3. Comandos básicos de Git.
4. Ramas, _branches_ y _forks_.
5. Cómo bajar, subir y comentar información.
6. Ejemplos de proyectos editoriales que usan Git.

## Conocimientos previos

* Uso básico de la terminal.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
