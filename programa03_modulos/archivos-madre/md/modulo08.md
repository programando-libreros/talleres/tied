# Mundo de los _ebooks_

* Horas: 2
* Tipo: teórico
* Relaciones: 1e, 2e, 3e, 4e, 5e, 6e, 7e, 9e, 11e, 16e, 19e, 20e, 21e, 22e

## Descripción

Un _ebook_ y un +++EPUB+++ no son lo mismo aunque comparten una
relación en común: la producción de una publicación electrónica.
Un +++EPUB+++ es un conjunto de documentos +++XHTML+++ comprimidos
en un archivo +++ZIP+++ para su portabilidad y su legibilidad.
En este módulo se hará un análisis teórico sobre el desarrollo
de las publicaciones estandarizadas y propietarias, como los
formatos de Amazon o de Apple.

## Objetivos

* Aprender qué es una publicación estandarizada.
* Conocer la diferencia entre los tipos de _ebooks_.
* Mostrar herramientas para hacer _ebooks_.

## Temas

1. ¿Qué es una publicación estandarizada?
2. ¿Qué es un _ebook_?
3. ¿Qué es un +++EPUB+++?
4. ¿Cuáles otros formatos pueden ser _ebook_?
5. ¿Cuáles herramientas existen para producir _ebooks_?
6. ¿Dónde se puede distribuir _ebooks_?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.
