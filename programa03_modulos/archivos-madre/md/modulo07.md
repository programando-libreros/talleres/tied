# Digitalización: del impreso al digital

* Horas: 5
* Tipo: teórico-práctico
* Relaciones: 6e, 8e, 11r, 16e, 22e

## Descripción

Este módulo se enfocará en el traslado del impreso al digital.
Se conocerán los pasos necesarios que nos facilitarán el trabajo
de la extracción del texto y la obtención de un +++PDF+++ a partir
del levantamiento de imágenes hechas con un escáner. Además,
se conocerán diferentes tipos de escáneres, el _software_ necesario
para el posprocesamiento y las medidas de calidad pertinentes
para la adquisición de un producto final.

## Objetivos

* Instalar ScanTailor.
* Instalar Simple Scan.
* Realizar la nomenclatura correcta en las carpetas.
* Aprender a hacer un levantamiento de imágenes.
* Llevar a cabo el posprocesamiento de imágenes.
* Hacer un reconocimiento de caracteres y obtener un archivo de texto.
* Conocer las herramientas para compilar archivos +++PDF+++.
* Reflexionar sobre la compartición de materiales y acercarnos a las bibliotecas libres que lo permiten.

## Temas

1. ¿Qué es ScanTailor y cómo funciona?
2. Cómo nombrar carpetas
3. ¿Qué es el posprocesamiento de imágenes?
4. Uso de +++OCR+++ para la obtención de texto.
5. Compilación de imágenes a un +++PDF+++ final.
6. Ejemplos bibliotecas libres. 

## Conocimientos previos

Ninguno.

## Materiales necesarios

* Escáner de cama plana.
* Computadoras para instalar los programas.
* Conexión a internet.
* Cañón.
