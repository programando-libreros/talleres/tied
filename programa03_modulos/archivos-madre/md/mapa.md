# Mapa curricular

En la siguiente gráfica se ven las relaciones entre los módulos
que comprenden la currícula del presente programa. Por motivos
de clarificación, la forma de cada nodo corresponde a su tipo:

* Nodo circular: módulo práctico
* Nodo triangular: módulo teórico
* Nodo en diamante: módulo teórico-práctico

El tamaño de cada nodo es relativo a la carga horaria del módulo.
Además, los colores y diseños de los vectores señalan sus tipos
de relación:

* Vector grueso: relación necesaria
* Vector normal: relación recomendada
* Vector discontinuo: relación emparentada

Los módulos emparentados pueden interesar al estudiante, pero
carecen de relación causal para el cumplimiento de la currícula.
Los módulos con relación recomendada tienen nexos causales que
pueden obviarse, aunque su cumplimiento facilitaría la ejecución
del módulo condicionado. Por último, la relación necesaria es
un vínculo causal que no puede eludirse.

Cabe observar que en la ficha de cada módulo se abrevia el tipo
de relación. Por ejemplo, la relación `2e` del módulo 1 denota
una relación **e**mparentada con el módulo 2.

Los números de cada nodo hacen mención al módulo correspondiente:

@modules

![](../archivos-madre/img/graph.png)
