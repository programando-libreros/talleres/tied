# Artículos académicos en +++XML JATS+++ I

* Horas: 5
* Tipo: teórico-práctico
* Relaciones: 2e, 2r, 4e, 5e, 5r, 6e, 8e, 17r

## Descripción

Hoy en día para la publicación de artículos académicos en repositorios
científicos es necesario producir archivos en formato +++XML+++
y +++PDF+++. Uno de los esquemas más populares es +++JATS+++,
el cual con ciertas modificaciones es usado por Sci+++ELO+++.
En este módulo se explicará la importancia de este tipo de formatos
y su esquema básico, así como las herramientas y las metodologías
que pueden emplearse para evitar hacer un doble trabajo ---la
estructuración del +++XML+++ y la maquetación del +++PDF+++---.
Con un enfoque de lo simple a lo complejo, se mostrará cómo es
posible pasar de lenguajes de marcado ligero, como Markdown,
a archivos +++XML+++ y, de ahí, a su importación para la producción
de +++PDF+++.

## Objetivos

* Conocer qué es un archivo +++XML+++.
* Aprender qué es un esquema +++XML+++, con énfasis en los esquemas
  +++JATS+++ y Sci+++ELO+++.
* Explicar un enfoque de trabajo que permita incluir en un mismo
  ciclo de producción el desarrollo de archivos +++XML+++ y +++PDF+++.

## Temas

1. ¿Qué son los archivos +++XML+++ y dónde se localizan dentro
   de la familia +++HTML+++?
2. ¿Qué son los esquemas +++XML+++ +++JATS+++ y Sci+++ELO+++?
3. Propuesta metodológica de lo simple a lo complejo: de +++MD+++
   a +++XML+++ y +++PDF+++ hecho con InDesign o TeX.
4. Realización de ejercicios básicos para obtener archivos +++XML+++
   y +++PDF+++.

## Conocimientos previos

* Estar familiarizado con los lenguajes de marcado.
* Comprender la relevancia general de los repositorios científicos.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
