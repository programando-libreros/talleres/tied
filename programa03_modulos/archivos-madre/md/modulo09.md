# Mundo de los _appbooks_

* Horas: 2
* Tipo: teórico
* Relaciones: 8e, 11e, 19e, 20e

## Descripción

Con el advenimiento y desarrollo de las tecnologías _web_ nos
encontramos en un mundo diferente con respecto a la visualización
y el manejo de la información. En este contexto es donde nacen
las publicaciones no estandarizadas. Las aplicaciones y los _appbooks_
son producciones digitales que se complementan con el usuario
a través de una dinámica interactiva. Estas publicaciones se
basan en distintas tecnologías para ofrecernos la posibilidad
de desarrollar objetos digitales que brindan narrativas poco
convencionales y permiten nuevas experiencias de lectura. En
este módulo se describirán los tipos de publicación no estandarizada,
su funcionamiento y las herramientas más utilizadas para su elaboración.

## Objetivos

* Aprender qué es una publicación no estandarizada.
* Conocer la diferencia entre los tipos de _appbooks_.
* Mostrar herramientas para hacer _appbooks_.

## Temas

1. ¿Qué es una publicación no estandarizada?
2. ¿Qué es un _appbook_?
3. ¿Cuáles formatos pueden ser _appbook_?
4. ¿Cuáles herramientas existen para producir _appbooks_?
5. ¿Dónde se puede distribuir _appbooks_?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Cañón.
