# Bash: introducción al uso de la terminal

* Horas: 5
* Tipo: práctico
* Relaciones: 2e, 17r

## Descripción

La producción de publicaciones y la gestión de proyectos editoriales
por lo general se dan a través de interfaces gráficas. Pese a
su facilidad de uso, esto provoca inconvenientes que afectan
la calidad de los productos editoriales. El empleo de la terminal
---es decir, la ausencia de entornos gráficos--- permite tener
un mayor control sobre los documentos y los flujos de trabajo.
En este módulo se familiarizará al usuario con la terminal Bash
y sus funciones más sencillas para tener un mayor control sobre
los proyectos.

## Objetivos

* Instalar y configurar la terminal Bash.
* Aprender comandos básicos de Bash.
* Hacer ejercicios para reforzar el aprendizaje.

## Temas

1. ¿Qué son la interfaz de línea de comandos y Bash?
2. Instalación y configuración de Bash.
3. Uso de `ls`: lista de ficheros.
4. Uso de `pwd`: impresión de la ubicación.
5. Uso de `mkdir`: creación de directorios.
6. Uso de `cd`: cambio de directorio. 
7. Uso de `nano`: creación y edición de archivos. 
8. Uso de `cat`: exhibición de archivos y más. 
9. Uso de `mv`: movimiento o renombre de ficheros. 
10. Uso de `cp`: copia de ficheros. 
11. Uso de `rm`: eliminación de ficheros. 
12. Uso de `clear`: limpieza de la terminal. 

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras con Bash o +++GNU+++/Linux instalado.
* Conexión a internet.
* Cañón.
