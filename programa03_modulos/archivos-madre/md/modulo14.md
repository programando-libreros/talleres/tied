# Bienes comunes y _anticopyright_

* Horas: 10
* Tipo: teórico
* Relaciones: 13e, 15e, 16e

## Descripción

Ante el engrosamiento de las legislaciones relativas a la propiedad
intelectual, varios académicos y activistas han puesto énfasis
en tratar las producciones culturales como bienes comunes. En
este módulo se analizarán las críticas, las propuestas y los
límites discursivos expuestos por grupos u organizaciones a favor
de los bienes comunes, como son los movimientos del _software_
y la cultura libres, las iniciativas del código y el acceso abiertos
y otras posturas críticas. La reflexión se llevará a cabo a través
de los conceptos de _copyright_, _copyleft_, _copyjustright_,
_copyfarleft_ y _anticopyright_.

## Objetivos

* Conocer una breve historia del movimiento contemporáneo en pos
  de los bienes comunes.
* Aprender las diferencias entre _copyright_, _anticopyright_,
  _copyleft_, _copyjustright_ y _copyfarleft_.
* Reflexionar sobre los supuestos, las posibilidades y los límites
  de los movimientos e iniciativas en pos de los bienes comunes.

## Temas

1. El familiar incómodo de la propiedad intelectual: los bienes
   comunes.
2. El resurgimiento de la defensa a los bienes comunes: el
   _software_ libre.
3. La bifurcación: el código abierto.
4. La amplificación: la cultura libre y el acceso abierto.
5. La crítica interna: el _copyfarleft_.
6. En la búsqueda de los bienes comunes.
7. Historización: ¿qué puede decirnos Proudhon y Walter Benjamin
   en pos de los bienes comunes?

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Ninguno.
