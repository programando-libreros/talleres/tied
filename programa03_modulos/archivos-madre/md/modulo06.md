# Pecas: herramientas editoriales desde la terminal

* Horas: 6
* Tipo: práctico
* Relaciones: 1e, 2e, 3e, 4e, 7e, 8e, 17r, 18n, 22e

## Descripción

Hacer un +++EPUB+++ nunca había sido tan fácil como lo es ahora.
En un entorno técnico y según el método ramificado de edición
es como surgió Pecas: una serie de *scripts* que automatizan
el quehacer editorial y que pueden marcar una diferencia en su
calidad. En este modulo se conocerá a fondo la utilización de
esta herramienta y el uso de sus parámetros según las necesidades
de cada proyecto editorial.

## Objetivos

* Instalar Pecas.
* Introducir cada una de las herramientas editoriales que conforman Pecas.
* Conocer los parámetros necesarios para su funcionamiento.
* Aprender qué son los metadatos y cuál es su sintaxis correcta.
* Reflexionar acerca de esta metodología.
* Comprender las ventajas al optar por este flujo de trabajo.

## Temas

1. ¿Qué es Pecas?
2. ¿Qué es un árbol de directorios?
3. Rutas relativas.
4. Revisión de parámetros: de lo sencillo a lo complejo.
5. ¿Qué es el archivo +++YAML+++ y cómo es su sintaxis?
6. Verificación de +++EPUB+++.
7. Estado de Pecas y sus dependencias: `pc-doctor`.

## Conocimientos previos

* Uso básico de la terminal.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
