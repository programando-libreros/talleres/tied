# Lenguajes de marcado: +++MD+++ y +++HTML+++

* Horas: 7
* Tipo: teórico-práctico
* Relaciones: 1e, 3e, 4e, 5e, 6e, 8e, 10e, 18e

## Descripción

Un lenguaje de marcado es una manera de darle estructura a un
texto. En este módulo aprenderemos a aplicar este tipo de sintaxis
a las publicaciones para que sean fáciles de leer y entendibles
para cualquier computadora. Para ello, se conocerán las ventajas
y desventajas de trabajar con Markdown y +++HTML+++, se pondrán
a prueba dependiendo de cada una de las necesidades del proyecto
editorial y se acostumbrará a trabajar las estructuras antes
que el diseño.

## Objetivos

* Aprender a utilizar editores de texto.
* Conocer qué son los lenguajes de marcado.
* Usar de sintaxis de Markdown.
* Usar de etiquetas básicas en +++HTML+++.
* Realizar una publicación con +++MD+++ o +++HTML+++.

## Temas

1. Cómo descargar y utilizar un editor de textos.
2. Nomenclatura para hacer uso de carpetas.
3. Introducción y sintaxis a Markdown.
4. Introducción y sintaxis a +++HTML+++. 
5. Elaboración de una publicación.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
