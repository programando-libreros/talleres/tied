# Diseño +++CSS+++ para publicaciones digitales

* Horas: 7 
* Tipo: práctico
* Relaciones: 2r, 8e

## Descripción

Las hojas de estilos +++CSS+++ dan pauta a un mejor diseño de
las páginas +++HTML+++. Estas nos ayudan a modificar colores,
tamaños, fuentes, fondos o sombras y la posición de distintos
elementos. Este módulo se enfocará en el desarrollo de una hoja
de estilos personalizada e indispensable para la obtención de
una publicación electrónica equilibrada y agradable a la vista.
Además, se enseñarán sus ventajas y desventajas, sus partes básicas
y su declaración en el archivo +++HTML+++.

## Objetivos

* Conocer los atributos de una hoja de estilos.
* Vincular hojas +++CSS+++ con el archivo +++HTML+++.
* Explicar los atributos y valores.
* Usar la sintaxis correcta para la aplicación de elementos.
* Mostrar algunos atributos.

## Temas

1. Cómo declarar nuestro archivo +++CSS+++.
2. Selectores y reglas visuales.
3. Sintaxis de propiedades +++CSS+++.
4. Colores y tipografías.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
