# Bibliotecas libres: LibGen, Aaaaarg, Sci-Hub y más

* Horas: 4 
* Tipo: teórico-práctico
* Relaciones: 7e, 8e, 13e, 14e, 15e

## Descripción

Los repositorios privados constriñen el acceso y se valen de
plataformas «especializadas» solo solventables para ciertas instituciones.
En este contexto surgen las bibliotecas libres que combaten la
centralización y el control total de los derechos de autor por
parte del sector privado. En estos espacios se almacenan textos
literarios, filosóficos, feministas, anarquistas y científicos.
El trabajo de estas plataformas es muy similar porque comparten
un objetivo: la defensa del acceso abierto. En este módulo se
hará un recorrido sobre el surgimiento de estas librerías y se
conocerán los parámetros necesarios para subir o descargar obras.

## Objetivos

* Conocer cuáles son las bibliotecas libres.
* Aprender a subir publicaciones.
* Aprender a bajar información.
* Reflexionar sobre la liberación de contenidos a través de estos espacios.

## Temas

1. ¿Qué es una biblioteca libre?
2. Cómo subir y bajar material de estas plataformas.
3. ¿Qué tipo de formatos permite subir o descargar? 
4. Comprensión de la labor e importancia de estas bibliotecas.

## Conocimientos previos

* Ninguno.

## Materiales necesarios

* Computadoras para realizar los ejercicios.
* Conexión a internet.
* Cañón.
