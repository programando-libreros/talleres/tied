\babel@toc {spanish}{}
\contentsline {chapter}{Descripción}{1}{chapter*.1}% 
\contentsline {section}{Antecedentes}{2}{section*.2}% 
\contentsline {section}{Proyección}{3}{section*.2}% 
\contentsline {chapter}{Información general}{4}{chapter*.3}% 
\contentsline {section}{Objetivos}{4}{section*.4}% 
\contentsline {section}{Propuesta de cronograma}{5}{Item.8}% 
\contentsline {section}{Requisitos previos}{6}{Item.18}% 
\contentsline {section}{Necesidades espaciales}{6}{Item.18}% 
\contentsline {chapter}{\emph {Syllabus}}{7}{chapter*.5}% 
\contentsline {section}{Sesión 1: Más allá de Adobe}{7}{section*.6}% 
\contentsline {section}{Sesión 2: Bienvenido a la edición «desde cero»}{8}{Item.23}% 
\contentsline {section}{Sesión 3: ¡Esto es Pecas! \textgreater {} Herramientas editoriales libres}{8}{Item.25}% 
\contentsline {section}{Sesión 4: De la planicie y los tornados al bosque y las ramas}{9}{Item.29}% 
\contentsline {section}{Sesión 5: Bienvenidos al mundo de Markdown}{10}{Item.36}% 
\contentsline {section}{Sesión 6: Nuestro primer proyecto con Markdown}{11}{Item.42}% 
\contentsline {section}{Sesión 7: ¿Otra vez Pecas?}{11}{Item.46}% 
\contentsline {section}{Sesión 8: Mi primer \textsc {epub}}{12}{Item.51}% 
\contentsline {section}{Sesión 9: Viaje a la exósfera}{13}{Item.55}% 
\contentsline {section}{Sesión 10: Regreso a la tierra}{13}{Item.63}% 
\contentsline {chapter}{Costos}{15}{chapter*.7}% 
