# Márgenes filosóficos para repensar la propiedad intelectual

Ramiro Santa Ana Anguiano
Melisa Pacheco Bayardo

A partir de finales del siglo XIX la producción cultural, entre ella la
filosófica, empieza a protegerse a través de leyes de propiedad. En este rubro,
el siglo XX se caracterizó por una sistematización y unificación de las
distintas legislaciones para dar nacimiento a la propiedad intelectual (PI).
En nuestro siglo, a la par que se busca un robustecimiento cuantitativo de la
PI, se pretende constituir una teoría que la justifique.

Varios críticos a favor y en contra han notado que el aumento de la relevancia
económica de la PI está relacionada con el advenimiento de las nuevas
tecnologías de la información y la comunicación (TIC). No es coincidencia, ambos
espectros asienten en un desfase entre las nuevas técnicas de reproducción y
su regulación jurídica. Pero la discrepancia viene en si estas técnicas tienen
que obedecer a lo dictaminado por las leyes o si estas han de adaptarse a los
nuevos cambios tecnológicos.

En «las guerras» de la PI las consecuencias son sociales y culturales, aunque
gran parte del debate se ha dado en los terrenos económico, político y jurídico.
Entre uno y otro «bando» se discute la manera adecuada de equilibrar ese
desfase.

Sin embargo, existe otro desfase. Los marcos teóricos con los que habíamos
comprendido a la producción intelectual no se ajustan ya a las TIC ni a las
técnicas de reproducción ni a las necesidades jurídicas. Desde una perspectiva
histórica, filosófica y (post)humanista ¿es posible repensar y proponer nuevos
marcos para comprender este fenómeno de nuestro quehacer cultural contemporáneo?

En este seminario se plantearán las bases necesarias para comenzar con esta
reflexión.

## Objetivos generales:

1. Explicar los supuestos filosóficos de la propiedad intelectual.
2. Presentar una crítica hacia los bienes comunes desde una mirada latinoaméricana.
3. Mostrar un panorama filosófico de los derechos de autor.
4. Exponer las posibilidades, límites y dificultades más allá de los derechos de autor.

## Objetivos por unidad:

1.1. Dar a conocer las tres dimensiones de la Propiedad Intelectual.
1.2. Esclarecer la teoría utilitarista de la Propiedad Intelectual.
1.3. Conocer la teoría basada en Hegel y Kant de la Propiedad Intelectual.
1.4. Emprender un análisis de la teoría lockeana de la Propiedad Intelectual.
2.1. Explicar las distintas facetas de los bienes comunes.
2.2. Definir el supuesto conflicto entre los bienes comunes y la Propiedad Intelectual.
2.3. Demostrar el traslado del movimiento del *software* libre a la iniciativa del código abierto y la cultura libre.
2.4. Supuestos filosóficos de la cultura libre y apertura a la edición libre.
3.1. Especificar el marco teórico y filosófico de los derechos de autor.
3.2. Interpretar la historia de los derechos de autor.
3.3. Comentar las similitudes y las diferencias de los derechos de autor.
3.4. Comprensión del panorama de los derechos de autor en México.
4.1. Describir alternativas a derechos de autor contemporáneos.
4.2. Narrar las críticas internas al *copyleft*.
4.3. Revelar el paso de la discusión teórica a la práctica cultural.
4.4. Exhibir formas de trabajo «libres».

|----------------------------|
|       Índice temático      |
|----------------------------|
|Unidad|       Tema   	     |
|------|---------------------|
|1     |Propiedad Intelectual|  
|2     |Bienes comunes		 |  
|3     |Derechos de autor    |   
|4     |*Copyleft*           |
|----------------------------|

|---------------------------------------------------------|
|Unidad|              Tema y subtemas                  	  |
|------|--------------------------------------------------|
|1     |1.1. ¿Qué es la propiedad Intelectual?     	      |	  
|      |1.2. Teoría progresista                   	      |
|      |1.3. Teoría personalista                  	      |  
|      |1.4. Teoría laborista				      	      |
|---------------------------------------------------------| 
|2     |2.1. ¿Qué son los bienes comunes?         	      |
|      |2.2. Supuesta guerra                      	      |
|      |2.3. Orígenes actuales                    	      |
|      |2.4. Camino a una cultura y edición libres	      |
|---------------------------------------------------------|
|3     |3.1. ¿Qué son los derechos de autor?              |
|      |3.2. Breve historia de los derechos de autor      |
|      |3.3. *Copyright* y derechos de autor              |
|      |3.4. Derechos de autor en México                  |
|---------------------------------------------------------|
|4     |4.1. ¿Qué es el *copyleft*?                       |
|      |4.2. Del *copyleft* al *copyfarleft* y *copyfight*|
|      |4.3. Licencias de uso                             |
|      |4.4. Demostración de digitalización               |
|---------------------------------------------------------|
