% universal settings
\documentclass[smalldemyvopaper,11pt,oneside,extrafontsizes]{memoir}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[spanish]{babel}
\usepackage[osf]{Alegreya,AlegreyaSans}

% PACKAGE DEFINITION
% typographical packages
\usepackage{microtype} % for micro-typographical adjustments
\usepackage{setspace} % for line spacing
\usepackage{lettrine} % for drop caps and awesome chapter beginnings
\usepackage{titlesec} % for manipulation of chapter titles
\usepackage{enumitem} % for lists

% other
\usepackage{calc}
\usepackage{hologo}
\usepackage{hyperref}
%\usepackage[hidelinks]{hyperref}
%\usepackage{showframe}

% Bibliography
\usepackage[style=apa,backend=biber]{biblatex} % for bibliography
%\bibliography{bibliografia.bib}

% PHYSICAL DOCUMENT SETUP
% media settings
\setstocksize{8.5in}{5.675in}
\setulmarginsandblock{0.611in}{1.222in}{*}

% defining the title and the author
%\title{\LaTeX{} ePub Template}
%\title{\textsc{how i started to love {\fontfamily{cmr}\selectfont\LaTeX{}}}}
\title{Márgenes filosóficos para repensar la propiedad intelectual}
\author{Ramiro Santa Ana Anguiano}
\newcommand{\ISBN}{0-000-00000-2}
\newcommand{\press}{\textsc{unam-ff}y\textsc{l}}

% custom second title page
\makeatletter
\newcommand*\halftitlepage{\begingroup % Misericords, T&H p 153
	\setlength\drop{0.1\textheight}
	\begin{center}
		\vspace*{\drop}
		\rule{\textwidth}{0in}\par
		{\large\textsc\thetitle\par}
		\rule{\textwidth}{0in}\par
		\vfill
	\end{center}
	\endgroup}
\makeatother

% chapter title display
\titleformat
{\chapter}
[display]
{\normalfont\scshape\Large}
{\Huge\thechapter\centering}
{0pt}
{\centering}[]

% typographical settings for the body text
\setlength{\parskip}{0em}
\linespread{1.09}

% HEADER AND FOOTER MANIPULATION
% for normal pages
\nouppercaseheads
\headsep = 0.16in
\makepagestyle{mystyle} 
\setlength{\headwidth}{\dimexpr\textwidth+\marginparsep+\marginparwidth\relax}
\makerunningwidth{mystyle}{\headwidth}
\makeevenhead{mystyle}{}{\textsf{\scriptsize\scshape\thetitle}}{}
\makeoddhead{mystyle}{}{\textsf{\scriptsize\scshape Márgenes filosóficos para repensar la propiedad intelectual}}{}
\makeevenfoot{mystyle}{}{}{}
\makeoddfoot{mystyle}{}{\textsf{\scriptsize\thepage}}{}
\makeatletter
\makepsmarks{mystyle}{%
	\createmark{chapter}{left}{nonumber}{\@chapapp\ }{.\ }}
\makeatother
% for pages where chapters begin
\makepagestyle{plain}
\makerunningwidth{plain}{\headwidth}
\makeevenfoot{plain}{}{}{}
\makeoddfoot{plain}{}{}{}
\pagestyle{mystyle}
% END HEADER AND FOOTER MANIPULATION

% table of contents customisation
\renewcommand\contentsname{\normalfont\scshape Índice}
\renewcommand\cftchapterfont{\normalfont}
\renewcommand{\cftchapterpagefont}{\normalfont}
\renewcommand{\printtoctitle}{\centering\Huge}

% layout check and fix
\checkandfixthelayout
\fixpdflayout

% BEGIN THE DOCUMENT
\begin{document}

\chapter*{Márgenes filosóficos para repensar la propiedad intelectual}

\begin{center}
	Ramiro Santa Ana Anguiano \\
	Melisa Pacheco Bayardo
\end{center}

\vskip 3em

\noindent A partir de finales del siglo \textsc{xix} la producción cultural, entre ella la
filosófica, empieza a protegerse a través de leyes de propiedad. En este
rubro, el siglo \textsc{xx} se caracterizó por una sistematización y unificación
de las distintas legislaciones para dar nacimiento a la propiedad
intelectual (\textsc{pi}). En nuestro siglo, a la par que se busca un
robustecimiento cuantitativo de la \textsc{pi}, se pretende constituir una teoría
que la justifique.

Varios críticos a favor y en contra han notado que el aumento de la
relevancia económica de la \textsc{pi} está relacionada con el advenimiento de
las nuevas tecnologías de la información y la comunicación (\textsc{tic}). No es
coincidencia, ambos espectros asienten en un desfase entre las nuevas
técnicas de reproducción y su regulación jurídica. Pero la discrepancia
viene en si estas técnicas tienen que obedecer a lo dictaminado por las
leyes o si estas han de adaptarse a los nuevos cambios tecnológicos.

En «las guerras» de la \textsc{pi} las consecuencias son sociales y culturales,
aunque gran parte del debate se ha dado en los terrenos económico,
político y jurídico. Entre uno y otro «bando» se discute la manera
adecuada de equilibrar ese desfase.

Sin embargo, existe otro desfase. Los marcos teóricos con los que
habíamos comprendido a la producción intelectual no se ajustan ya a las
\textsc{tic} ni a las técnicas de reproducción ni a las necesidades jurídicas.
Desde una perspectiva histórica, filosófica y (post)humanista ¿es
posible repensar y proponer nuevos marcos para comprender este fenómeno
de nuestro quehacer cultural contemporáneo?

En este seminario se plantearán las bases necesarias para comenzar con
esta reflexión.

\subsection*{Unidades}

\begin{center}
	\begin{tabular}{ | c | l | }
		\hline
		1 & Propiedad intelectual	\\\hline
		2 & Bienes comunes 			\\\hline
		3 & Derechos de autor 		\\\hline
		4 & \emph{Copyleft} 		\\\hline
	\end{tabular}
\end{center}

\subsection*{Objetivos generales}

\begin{enumerate}[noitemsep]
	\item
	  Explicar los supuestos filosóficos de la \textsc{pi}.
	\item
	  Presentar una crítica hacia los bienes comunes (\textsc{bc}) desde una mirada latinoamericana.
	\item
	  Mostrar un panorama de los derechos de autor.
	\item
	  Exponer las posibilidades, límites y dificultades más allá de los derechos de autor.
\end{enumerate}

\newpage

\subsection*{Objetivos por unidad}

\begin{enumerate}[noitemsep]
	\item [1.1]
		Dar a conocer las tres dimensiones de la \textsc{pi}.
	\item [1.2]
		Esclarecer la teoría utilitarista de la \textsc{pi}.
	\item [1.3]
		Conocer la teoría basada en Hegel y Kant de la \textsc{pi}.
	\item [1.4]
		Emprender un análisis de la teoría lockeana de la \textsc{pi}.
\end{enumerate}

\begin{enumerate}[noitemsep]
	\item [2.1]
		Explicar las distintas facetas de los \textsc{bc}.
	\item [2.2]
		Definir el supuesto conflicto entre los \textsc{bc} y la \textsc{pi}.
	\item [2.3]
		Demostrar el traslado del movimiento del \emph{software} libre a la iniciativa del código abierto y la cultura libre.
	\item [2.4]
		Supuestos filosóficos de la cultura libre y apertura a la edición libre.
\end{enumerate}

\begin{enumerate}[noitemsep]
	\item [3.1]
		Especificar el marco teórico y filosófico de los derechos de autor.
	\item [3.2]
		Interpretar la historia de los derechos de autor.
	\item [3.3]
		Comentar las similitudes y las diferencias de los derechos de autor y el \emph{copyright}.
	\item [3.4]
		Comprensión del panorama de los derechos de autor en México.
\end{enumerate}

\begin{enumerate}[noitemsep]
	\item [4.1]
		Describir alternativas a los derechos de autor contemporáneos.
	\item [4.2]
		Narrar las críticas internas al \emph{copyleft}.
	\item [4.3]
		Revelar el paso de la discusión a la práctica cultural.
	\item [4.4]	
		Exhibir formas de trabajo «libres».
\end{enumerate}

\newpage

\subsection*{Temas}

\begin{center}
	\begin{tabular}{ | c | l | }
		\hline
		Unidad & Temas \\\hline
		1 & ¿Qué es la \textsc{pi}?	\\\hline
		1 & Teoría progresista de la \textsc{pi}	\\\hline
		1 & Teoría personalista de la \textsc{pi}	\\\hline
		1 & Teoría laborista de la \textsc{pi}	\\\hline
		2 & ¿Qué son los \textsc{bc}?	\\\hline
		2 & Supuesta «guerra» entre la \textsc{pi} y los \textsc{bc}	\\\hline
		2 & Orígenes actuales de los \textsc{bc}	\\\hline
		2 & Camino a una cultura y edición libres	\\\hline
		3 & ¿Qué son los derechos de autor?	\\\hline
		3 & Breve historia de los derechos de autor	\\\hline
		3 & Derechos de autor y \emph{copyright}\\\hline
		3 & Derechos de autor en México	\\\hline
		4 & ¿Qué es el \emph{copyleft}?	\\\hline
		4 & Del \emph{copyleft} al \emph{copyfarleft} y \emph{copyfight}	\\\hline
		4 & Licencias de uso	\\\hline
		4 & Demostración de digitalización de libro con \emph{software} libre	\\\hline
	\end{tabular}
\end{center}

\end{document}
% END THE DOCUMENT
